/**
 * Created by karan on 27-Feb-15.
 */
var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('index', { title: 'Heloo World' });
});

module.exports = router;