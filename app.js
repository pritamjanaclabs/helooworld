process.env.NODE_ENV = 'test';
config = require('config');
var express = require('express');
var path = require('path');
//var bodyParser = require('body-parser');
var http = require('http');
var routes = require('./routes/index');
var app = express();
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
//app.use(bodyParser.json());
//app.use(bodyParser.urlencoded({extended: false}));

app.use(express.static(path.join(__dirname, 'public')));

app.get('/',routes);

app.use(function (req, res, next) {
    var err = new Error('Sorry Page Not Found :P');
    err.status = 404;
    next(err);
});

http.createServer(app).listen(config.get('PORT'), function () {
    console.log("My Server listening on port " + config.get('PORT'));
});